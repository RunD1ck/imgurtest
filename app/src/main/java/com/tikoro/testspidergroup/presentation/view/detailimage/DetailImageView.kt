package com.tikoro.testspidergroup.presentation.view.detailimage

import com.tikoro.testspidergroup.entity.detailimage.Comment
import com.tikoro.testspidergroup.entity.detailimage.DetailImageItem
import com.tikoro.testspidergroup.presentation.view.BaseView
import com.tikoro.testspidergroup.presentation.view.ProgressView
import moxy.MvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

interface DetailImageView : BaseView, ProgressView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun onShowDetailImage(image: DetailImageItem)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun onShowComments(listComments: List<Comment>)
}