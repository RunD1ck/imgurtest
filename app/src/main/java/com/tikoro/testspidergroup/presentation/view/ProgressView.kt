package com.tikoro.testspidergroup.presentation.view

import moxy.MvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

interface ProgressView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun onShowProgress(show: Boolean)
}