package com.tikoro.testspidergroup.presentation.presenter

import android.util.Log
import com.tikoro.testspidergroup.model.interactor.ImgurInteractor
import com.tikoro.testspidergroup.presentation.view.detailimage.DetailImageView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter
import org.koin.core.KoinComponent
import org.koin.core.inject

/**
 * Presenter to manage @[DetailImageView]
 *
 */

@InjectViewState
class DetailImagePresenter(private val id: String) : MvpPresenter<DetailImageView>(),
    KoinComponent {

    private val interactor: ImgurInteractor by inject()

    override fun onFirstViewAttach() {
        getDetailImage(id)
        getBestComments(id)
    }

    //Private fun
    private fun getDetailImage(id: String): Disposable {
        return interactor
            .getDetailImage(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                viewState.onShowProgress(true)
            }
            .doAfterTerminate {
                viewState.onShowProgress(false)
            }
            .subscribe({ detailImage ->
                viewState.onShowDetailImage(detailImage)
            }, {
                Log.e("Error", it.message.toString())
            })
    }

    private fun getBestComments(id: String): Disposable {
        return interactor
            .getBestComments(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ listComments ->
                viewState.onShowComments(listComments)
            }, {
                Log.e("Error", it.message.toString())
            })
    }

}
