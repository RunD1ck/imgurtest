package com.tikoro.testspidergroup.presentation.view.allimage

import androidx.paging.PagedList
import com.tikoro.testspidergroup.entity.allimages.ImageItem
import com.tikoro.testspidergroup.entity.dto.ImageDTO
import moxy.MvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

interface AllImageView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun onShowImages(listImages: PagedList<ImageItem>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun onShowDetailImage(id: String)
}