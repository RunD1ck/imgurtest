package com.tikoro.testspidergroup.presentation.presenter

import android.util.Log
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.tikoro.testspidergroup.model.interactor.ImgurInteractor
import com.tikoro.testspidergroup.presentation.view.allimage.AllImageView
import com.tikoro.testspidergroup.ui.common.pagination.ImgurDataSource
import io.reactivex.disposables.CompositeDisposable
import moxy.InjectViewState
import moxy.MvpPresenter
import org.koin.core.KoinComponent
import org.koin.core.inject

@InjectViewState
class AllImagePresenter : MvpPresenter<AllImageView>(), KoinComponent {
    private val interactor: ImgurInteractor by inject()
    private val compositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        getImages()
    }

    fun onImageItemClick(id: String) {
        viewState.onShowDetailImage(id)
    }

    private fun getImages() {
        val pagedListLiveData =
            LivePagedListBuilder(
                ImgurDataSource.Factory(interactor, compositeDisposable), PagedList
                    .Config
                    .Builder()
                    .setPageSize(40)
                    .setPrefetchDistance(3)
                    .setEnablePlaceholders(false)
                    .build()
            ).build()
        pagedListLiveData.observeForever {
            viewState.onShowImages(it)
        }
    }
}
