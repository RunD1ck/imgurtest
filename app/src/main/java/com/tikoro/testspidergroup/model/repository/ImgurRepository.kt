package com.tikoro.testspidergroup.model.repository

import com.tikoro.testspidergroup.model.data.server.ImgurApi


/**
 * Data source for Imgur
 *
 */
class ImgurRepository(private val api: ImgurApi) {


    /**
     * Get images from network request
     *
     * @param page - the data paging number
     * @param showViral - Show or hide viral images from the user section
     * @param mature - Show or hide mature images in the response section
     * @param albumPreviews - Include image metadata for gallery posts which are albums
     */
    fun getImages(page: Int, showViral: Boolean, mature: Boolean, albumPreviews: Boolean) =
        api.getGalleryImages(page, showViral, mature, albumPreviews)

    /**
     * Get detail info about current image from network request
     *
     * @param id - image hash
     */
    fun getDetailImage(id: String) = api.getDetailImage(id)

    /**
     * Get best comments from network request
     *
     * @param id - image hash
     */
    fun getComments(id: String) = api.getComments(id)
}