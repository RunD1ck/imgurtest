package com.tikoro.testspidergroup.model.data.server

import com.tikoro.testspidergroup.entity.detailimage.Comment
import com.tikoro.testspidergroup.entity.dto.BaseResponse
import com.tikoro.testspidergroup.entity.dto.ImageDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Retrofit2 interface to access Imgur API
 *
 */

interface ImgurApi {

    @GET("/3/gallery/top/top/day/{page}")
    fun getGalleryImages(
        @Path("page") page: Int,
        @Query("showViral") showViral: Boolean,
        @Query("mature") mature: Boolean,
        @Query("album_previews") albumPreviews: Boolean
    ): Single<BaseResponse<List<ImageDTO>>>

    @GET("/3/gallery/{id}")
    fun getDetailImage(
        @Path("id") id: String
    ): Single<BaseResponse<ImageDTO>>

    @GET("/3/gallery/{id}/comments")
    fun getComments(
        @Path("id") id: String
    ): Single<BaseResponse<List<Comment>>>
}