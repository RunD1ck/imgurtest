package com.tikoro.testspidergroup.model.interactor

import android.util.Log
import com.tikoro.testspidergroup.entity.dto.toDetailImageItem
import com.tikoro.testspidergroup.entity.dto.toImageItem
import com.tikoro.testspidergroup.model.repository.ImgurRepository

/**
 * Business logic and mappers for imgur
 *
 */
class ImgurInteractor(private val imgurRepository: ImgurRepository) {

    /**
     * Get and map images from @[ImgurRepository]
     *
     * @param page - the data paging number
     * @param showViral - Show or hide viral images from the user section
     * @param mature - Show or hide mature images in the response section
     * @param albumPreviews - Include image metadata for gallery posts which are albums
     */
    fun getImages(
        page: Int,
        showViral: Boolean = true,
        mature: Boolean = false,
        albumPreviews: Boolean = false
    ) =
        imgurRepository.getImages(page, showViral, mature, albumPreviews).map {
            it.data.map { imageDto ->
                imageDto.toImageItem()
            }
        }


    /**
     * Get and map detail image from @[ImgurRepository]
     *
     * @param id - image hash
     */
    fun getDetailImage(id: String) = imgurRepository.getDetailImage(id).map {
        it.data.toDetailImageItem()
    }

    /**
     * Get and map best comments from @[ImgurRepository]
     *
     * @param id - image hash
     */
    fun getBestComments(id: String) = imgurRepository.getComments(id).map {
        it.data
    }
}