package com.tikoro.testspidergroup.di

import com.tikoro.testspidergroup.model.interactor.ImgurInteractor
import com.tikoro.testspidergroup.model.repository.ImgurRepository
import org.koin.dsl.module

val appModule = module {

    factory { ImgurRepository(get()) }
    factory { ImgurInteractor(get()) }
}