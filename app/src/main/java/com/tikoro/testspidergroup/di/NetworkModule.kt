package com.tikoro.testspidergroup.di

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.tikoro.testspidergroup.model.data.server.ImgurApi
import okhttp3.OkHttpClient
import com.tikoro.testspidergroup.BuildConfig.BASE_URL
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    factory { provideDefaultOkhttpClient(androidContext()) }
    single { provideChatterApiService(provideRetrofit(androidContext(), BASE_URL)) }
}

fun provideDefaultOkhttpClient(contex: Context): OkHttpClient.Builder {
    return OkHttpClient.Builder()
        .addInterceptor { chain ->
            val newBuilder = chain.request().newBuilder()
            newBuilder.addHeader("Authorization", "Client-ID e8b426c7ef8afab")

            return@addInterceptor chain.proceed(newBuilder.build())
        }
        .addInterceptor(ChuckerInterceptor(contex))
        .readTimeout(90, TimeUnit.SECONDS)
        .connectTimeout(90, TimeUnit.SECONDS)
}

fun provideRetrofit(contex: Context, baseUrl: String): Retrofit {
    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(provideDefaultOkhttpClient(contex).build())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
}

fun provideChatterApiService(retrofit: Retrofit): ImgurApi =
    retrofit.create(ImgurApi::class.java)