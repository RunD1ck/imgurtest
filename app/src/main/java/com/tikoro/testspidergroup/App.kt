package com.tikoro.testspidergroup

import android.app.Application
import com.tikoro.testspidergroup.di.appModule
import com.tikoro.testspidergroup.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {


    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    appModule, networkModule
                )
            )
        }
    }
}