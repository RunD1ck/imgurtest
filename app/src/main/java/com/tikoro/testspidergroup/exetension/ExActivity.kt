package com.tikoro.testspidergroup.exetension

import android.app.Activity
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Show progress when send requests
 *
 * @param show - is visible progressbar
 */
fun Activity.showProgress(show: Boolean) {
    this.progressBar.isVisible = show
}