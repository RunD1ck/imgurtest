package com.tikoro.testspidergroup.exetension


/**
 * Check link on mime type
 *
 */
fun String.isNotVideoOrGif() =
    this.substringAfterLast(".") != "mp4" &&
            this.substringAfterLast(".") != "gif"
