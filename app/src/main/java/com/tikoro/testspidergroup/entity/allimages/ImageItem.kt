package com.tikoro.testspidergroup.entity.allimages

import com.squareup.moshi.Json

data class ImageItem(
    val id: String,
    val link: String,
    val height: Int,
    val width: Int
)