package com.tikoro.testspidergroup.entity.allimages

import com.squareup.moshi.Json

data class ImageInfo(
    @field:Json(name = "link")
    val link: String?,
    @field:Json(name = "description")
    val description: String?
)