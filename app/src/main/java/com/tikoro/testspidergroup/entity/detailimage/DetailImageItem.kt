package com.tikoro.testspidergroup.entity.detailimage

data class DetailImageItem(
    val id: String,
    val description: String?,
    val link: String,
    val score: Int,
    val ups: Int,
    val downs: Int,
    val name: String,
    val time: Long,
    val views: Int,
    val title: String?
)