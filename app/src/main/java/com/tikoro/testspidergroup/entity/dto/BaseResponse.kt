package com.tikoro.testspidergroup.entity.dto

import com.squareup.moshi.Json

data class BaseResponse<T>(
    @field:Json(name = "data")
    val data: T,
    @field:Json(name = "success")
    val success: Boolean,
    @field:Json(name = "status")
    val status: Int
)