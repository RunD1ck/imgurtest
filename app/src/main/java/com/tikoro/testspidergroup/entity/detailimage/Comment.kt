package com.tikoro.testspidergroup.entity.detailimage

import com.squareup.moshi.Json

data class Comment(
    @field:Json(name = "author")
    val author: String,
    @field:Json(name = "comment")
    val comment: String,
    @field:Json(name = "datetime")
    val datetime: Long,
    @field:Json(name = "children")
    val children: List<Comment>,
    @field:Json(name = "ups")
    val ups: Int
)