package com.tikoro.testspidergroup.entity.detailimage

import com.squareup.moshi.Json

data class Tag(
    @field:Json(name = "display_name")
    val name: String
)