package com.tikoro.testspidergroup.entity.dto

import com.squareup.moshi.Json
import com.tikoro.testspidergroup.entity.allimages.ImageItem
import com.tikoro.testspidergroup.entity.allimages.ImageInfo
import com.tikoro.testspidergroup.entity.detailimage.DetailImageItem

data class ImageDTO(
    @field:Json(name = "id")
    val id: String,
    @field:Json(name = "description")
    val description: String?,
    @field:Json(name = "score")
    val score: Int,
    @field:Json(name = "images")
    val images: List<ImageInfo>?,
    @field:Json(name = "cover_width")
    val coverWidth: Int,
    @field:Json(name = "cover_height")
    val coverHeight: Int,
    @field:Json(name = "ups")
    val ups: Int,
    @field:Json(name = "downs")
    val downs: Int,
    @field:Json(name = "account_url")
    val name: String,
    @field:Json(name = "datetime")
    val time: Long,
    @field:Json(name = "views")
    val views: Int,
    @field:Json(name = "title")
    val title: String?
)

fun ImageDTO.toImageItem() = ImageItem(
    this.id,
    this.images?.firstOrNull()?.link ?: "",
    this.coverHeight,
    this.coverWidth
)

fun ImageDTO.toDetailImageItem() = DetailImageItem(
    this.id,
    this.images?.firstOrNull()?.description,
    this.images?.firstOrNull()?.link ?: "",
    this.score,
    this.ups,
    this.downs,
    this.name,
    this.time,
    this.views,
    this.title
)