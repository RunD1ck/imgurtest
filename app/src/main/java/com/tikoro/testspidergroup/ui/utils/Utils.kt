package com.tikoro.testspidergroup.ui.utils

import java.text.SimpleDateFormat
import java.util.*


/**
 * Converter millsecond to create date
 *
 * @return - created date
 */
fun Long.convertMillsToDate(): String =
    SimpleDateFormat("dd MMM HH:mm", Locale.getDefault()).format(Date(this * 1000))