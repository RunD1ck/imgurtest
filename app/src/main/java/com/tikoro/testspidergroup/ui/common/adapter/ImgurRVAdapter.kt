package com.tikoro.testspidergroup.ui.common.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager
import com.hannesdorfmann.adapterdelegates4.paging.PagedListDelegationAdapter
import com.tikoro.testspidergroup.entity.allimages.ImageItem
import com.tikoro.testspidergroup.entity.dto.ImageDTO
import com.tikoro.testspidergroup.ui.common.list.imgurAdapterDelegate

fun ImageItem.isSame(image: ImageItem) = this.id == image.id

class ImgurRVAdapter(onClick: (String) -> Unit) : PagedListDelegationAdapter<ImageItem>(
    AdapterDelegatesManager<List<ImageItem>>().addDelegate(
        imgurAdapterDelegate(onClick)
    ),
    object : DiffUtil.ItemCallback<ImageItem>() {
        override fun areItemsTheSame(oldItem: ImageItem, newItem: ImageItem) =
            oldItem.isSame(newItem)

        override fun areContentsTheSame(oldItem: ImageItem, newItem: ImageItem) =
            oldItem.isSame(newItem)

    }
)