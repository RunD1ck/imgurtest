package com.tikoro.testspidergroup.ui.common.list

import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.tikoro.testspidergroup.R
import com.tikoro.testspidergroup.entity.allimages.ImageItem

private val set = ConstraintSet()

fun imgurAdapterDelegate(onClick: (String) -> Unit) =
    adapterDelegate<ImageItem, ImageItem>(R.layout.rv_image_item) {

        val clItem: ConstraintLayout = findViewById(R.id.clItem)
        val ivImage: ImageView = findViewById(R.id.ivImage)

        bind {
            Glide.with(context)
                .load(item.link)
                .fitCenter()
                .placeholder(R.color.gray)
                .into(ivImage)


            val ratio = String.format("%d:%d", item.width, item.height)
            set.clone(clItem)
            set.setDimensionRatio(ivImage.id, ratio)
            set.applyTo(clItem)

            itemView.setOnClickListener {
                onClick(item.id)
            }
        }
    }