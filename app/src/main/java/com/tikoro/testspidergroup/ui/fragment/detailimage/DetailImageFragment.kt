package com.tikoro.testspidergroup.ui.fragment.detailimage

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.tikoro.testspidergroup.R
import com.tikoro.testspidergroup.entity.detailimage.Comment
import com.tikoro.testspidergroup.entity.detailimage.DetailImageItem
import com.tikoro.testspidergroup.presentation.presenter.DetailImagePresenter
import com.tikoro.testspidergroup.presentation.view.detailimage.DetailImageView
import com.tikoro.testspidergroup.ui.common.BaseFragment
import com.tikoro.testspidergroup.ui.common.list.commentsAdapterDelegate
import com.tikoro.testspidergroup.ui.utils.convertMillsToDate
import kotlinx.android.synthetic.main.fragment_detail_image.*
import moxy.ktx.moxyPresenter


class DetailImageFragment : BaseFragment(),
    DetailImageView {

    override val layoutResId = R.layout.fragment_detail_image
    private val args: DetailImageFragmentArgs by navArgs()

    private val presenter by moxyPresenter { DetailImagePresenter(args.imageId ?: "") }
    private val adapter: ListDelegationAdapter<List<Comment>> by lazy {
        ListDelegationAdapter<List<Comment>>(
            commentsAdapterDelegate()
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvComments.layoutManager = LinearLayoutManager(requireContext())
        rvComments.adapter = adapter

    }

    override fun onShowDetailImage(image: DetailImageItem) {

        tvLike.isVisible = true
        tvDislike.isVisible = true
        tvViews.isVisible = true


        Glide.with(requireContext())
            .load(R.drawable.avatar)
            .circleCrop()
            .into(ivAvatar)

        Glide.with(requireContext())
            .load(image.link)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.color.gray)
            .into(ivImage)

        tvName.text = image.name
        if (!image.description.isNullOrEmpty()) {
            tvDescription.text = image.description
        } else {
            tvDescription.text = image.title
        }
        tvDate.text = image.time.convertMillsToDate()
        tvLike.text = image.ups.toString()
        tvDislike.text = image.downs.toString()
        tvViews.text = image.views.toString()

    }

    override fun onShowComments(listComments: List<Comment>) {
        adapter.items = listComments
        adapter.notifyDataSetChanged()
    }

}