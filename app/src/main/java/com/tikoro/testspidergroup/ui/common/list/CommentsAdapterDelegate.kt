package com.tikoro.testspidergroup.ui.common.list

import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.tikoro.testspidergroup.R
import com.tikoro.testspidergroup.entity.detailimage.Comment
import com.tikoro.testspidergroup.ui.utils.convertMillsToDate

fun commentsAdapterDelegate() = adapterDelegate<Comment, Comment>(R.layout.rv_comment_item) {

    val ivAvatar: ImageView = findViewById(R.id.ivAvatar)
    val tvName: TextView = findViewById(R.id.tvName)
    val tvComment: TextView = findViewById(R.id.tvComment)
    val tvDate: TextView = findViewById(R.id.tvDate)
    val tvLike: TextView = findViewById(R.id.tvLike)

    bind {
        Glide.with(context)
            .load(R.drawable.avatar)
            .circleCrop()
            .into(ivAvatar)

        tvName.text = item.author
        tvComment.text = item.comment
        tvLike.text = item.ups.toString()
        tvDate.text = item.datetime.convertMillsToDate()

    }
}