package com.tikoro.testspidergroup.ui.common.pagination

import android.util.Log
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.tikoro.testspidergroup.entity.allimages.ImageItem
import com.tikoro.testspidergroup.entity.dto.ImageDTO
import com.tikoro.testspidergroup.exetension.isNotVideoOrGif
import com.tikoro.testspidergroup.model.interactor.ImgurInteractor
import io.reactivex.disposables.CompositeDisposable

class ImgurDataSource(
    private val interactor: ImgurInteractor,
    private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Long, ImageItem>() {
    override fun loadInitial(
        params: LoadInitialParams<Long>,
        callback: LoadInitialCallback<Long, ImageItem>
    ) {
        compositeDisposable.add(interactor.getImages(1).subscribe({
            Log.d("Qwerty", "loadInitial ${it.size}")
            callback.onResult(it.filter { item ->
                item.link.isNotVideoOrGif()
            }, null, 2)
        }, { throwable ->
            Log.e("Error", throwable.message.toString())
        }))
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, ImageItem>) {

        compositeDisposable.add(interactor.getImages(params.key.toInt()).subscribe({
            Log.d("Qwerty","loadAfter ${it.size}")

            callback.onResult(it, (params.key + 1))
        }, { throwable ->
            Log.e("Error", throwable.message.toString())
        }))
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, ImageItem>) {}

    class Factory(
        private val feedInteractor: ImgurInteractor,
        private val compositeDisposable: CompositeDisposable
    ) : DataSource.Factory<Long, ImageItem>() {
        override fun create(): DataSource<Long, ImageItem> {
            return ImgurDataSource(feedInteractor, compositeDisposable)
        }
    }
}