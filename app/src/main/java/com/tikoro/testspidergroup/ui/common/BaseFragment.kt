package com.tikoro.testspidergroup.ui.common

import android.os.Bundle
import android.view.*
import com.chuckerteam.chucker.api.internal.ui.MainActivity
import com.tikoro.testspidergroup.exetension.showProgress
import com.tikoro.testspidergroup.presentation.view.ProgressView
import moxy.MvpAppCompatFragment

abstract class BaseFragment : MvpAppCompatFragment(), ProgressView {

    abstract val layoutResId: Int?

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return layoutResId?.let { inflater.inflate(it, container, false) }
    }

    override fun onShowProgress(show: Boolean) {
        activity?.showProgress(show)
    }


    override fun onStart() {
        activity?.showProgress(false) //onFirstViewAttach call in presenter after start
        super.onStart()
    }

}