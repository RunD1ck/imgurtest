package com.tikoro.testspidergroup.ui.fragment.allimage

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.tikoro.testspidergroup.R
import com.tikoro.testspidergroup.entity.allimages.ImageItem
import com.tikoro.testspidergroup.presentation.view.allimage.AllImageView
import com.tikoro.testspidergroup.presentation.presenter.AllImagePresenter

import com.tikoro.testspidergroup.ui.common.BaseFragment
import com.tikoro.testspidergroup.ui.common.adapter.ImgurRVAdapter
import kotlinx.android.synthetic.main.fragment_all_image.*
import moxy.ktx.moxyPresenter


class AllImageFragment : BaseFragment(),
    AllImageView {

    private val presenter by moxyPresenter { AllImagePresenter() }

    override val layoutResId = R.layout.fragment_all_image

    private val adapter: ImgurRVAdapter by lazy {
        ImgurRVAdapter {
            presenter.onImageItemClick(it)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        layoutManager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
        rvAllImages.layoutManager = layoutManager
        rvAllImages.adapter = adapter

    }

    override fun onShowImages(listImages: PagedList<ImageItem>) {
        adapter.submitList(listImages)
    }

    override fun onShowDetailImage(id: String) {
        val action = AllImageFragmentDirections.actionAllImageFragmentToDetailImageFragment(id)
        findNavController().navigate(action)
    }

}